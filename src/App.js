import Input from "./components/Input"
import './App.css';
import ToDo from "./components/ToDo";

function App() {
  return (
    <div className="App">
        <Input/>
        <ToDo/>
      </div>
  );
}

export default App;
