import React from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { removeToDo } from "../redux/actions/todo";

const ToDo = () => {
  const {list} = useSelector((state) => state.addToDo);
  const dispatch = useDispatch()
  console.log(list,"list");
  return (
    <div>
      <ul>
        {
        list?.map((item,index)=>{
          return <li  key={index}>{item.title} 
          <span key={index} onClick={()=>dispatch(removeToDo(item.id))}>sill</span>
          </li>
        })
        }</ul>
    </div>
  );
};

export default ToDo;
