import { Button } from '@mui/material'
import React, { useState } from 'react'
import {addToDo} from "../redux/actions/todo"
import { useDispatch } from 'react-redux'

const Input = () => {
  const dispatch = useDispatch()
  const [title,setTitle]=useState("")
const handlerClick =()=>{
  let newToDo= {
    id:Math.random(1000),
    title:title
  }
  dispatch(addToDo(newToDo))
  setTitle("")
  
}
  return (
    <form style={{paddingTop:"20px"}}>
        <input onChange={(e)=>setTitle(e.target.value)} value={title}   type="text" placeholder='Text Əlavə edin'/>
        <Button disabled={!title} onClick={handlerClick} variant="contained" color="success">ADD</Button>
    </form>
  )
}

export default Input