import {ADD_TODO,REMOVE_TODO} from "../types"

export const addToDo =(data)=>{
    return{
        type:ADD_TODO,
        payload:data,
        // id:id
    }

}
export const removeToDo =(id)=>{
    return {
        type:REMOVE_TODO,
        payload:id 
    }
}
