import {createStore,combineReducers} from "redux"
import ToDoReducer from "./reducers/todo"

const reducers=combineReducers({
    addToDo:ToDoReducer,
})

const store =createStore(reducers);

export default store;