import {ADD_TODO,REMOVE_TODO} from "../types"

const defaultData ={
    list:[]
}   

const reducer=(state=defaultData,action)=>{
 switch (action.type) {
     case ADD_TODO:
       return {
           ...state,
         list:[...state.list,action.payload]
        }


     case REMOVE_TODO:
        const del = state.list.filter(element=>element.id !== action.payload)
        console.log(del,"eltac");
       return {
           ...state,
           list:del
        }
     default:
         return state;
 }
}

export default reducer; 